import React from 'react';
import { Counter } from './features/counter/Counter';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LoginPage from 'features/auth/pages/LoginPage';
import { AdminLayout } from 'components/Layout';
import { NotFound, PrivateRoute } from 'components/Common';
import { CssBaseline } from '@mui/material';
import Dashboard from 'features/dashboard/Dashboard';

function App() {
  return (
    <BrowserRouter>
      <CssBaseline />
      <Switch>
        <Route path="/login" component={LoginPage}></Route>
        <Route path="/dashboard" component={Dashboard}></Route>
        <PrivateRoute path="/admin" component={AdminLayout}></PrivateRoute>
        <Route component={NotFound}></Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
