import { Student } from './student';
export interface StatisticsProps {
  male: number;
  female: number;
  highMark: number;
  lowMark: number;
}
export interface RankingByCityProps {
  cityId: string;
  rankingList: Student[];
}
export interface DashboardProps {
  loading: boolean;
  statistics: StatisticsProps;
  highestStudentList: Student[];
  lowestStudentList: Student[];
  rankingByCityList: RankingByCityProps[];
}
