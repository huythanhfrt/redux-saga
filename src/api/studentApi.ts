import { ListParams, ListResponse, Student } from 'models';
import axiosClient from './axiosClient';

const studentApi = {
  getStudentList: (params: ListParams): Promise<ListResponse<Student>> => {
    return axiosClient.get('/students', { params });
  },
  getStudentById: (id: string): Promise<any> => {
    return axiosClient.get(`/students/${id}`);
  },
  addStudent: (information: Student): Promise<Student> => {
    return axiosClient.post('/students', information);
  },
  updateStudent: (information: Student): Promise<Student> => {
    return axiosClient.patch(`/students/${information.id}`, information);
  },
  removeStudent: (id: string): Promise<any> => {
    return axiosClient.delete(`/students/${id}`);
  },
};
export default studentApi;
