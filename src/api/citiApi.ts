import { City, ListResponse } from './../models';

import axiosClient from './axiosClient';

const cityApi = {
  getAllCity: (): Promise<ListResponse<City>> => {
    return axiosClient.get('/cities', {
      params: {
        _page: 1,
        _limit: 10,
      },
    });
  },
};
export default cityApi;
