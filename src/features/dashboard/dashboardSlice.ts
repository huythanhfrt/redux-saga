import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { DashboardProps, RankingByCityProps, StatisticsProps, Student } from 'models';
const initialState: DashboardProps = {
  loading: false,
  statistics: {
    male: 0,
    female: 0,
    highMark: 0,
    lowMark: 0,
  },
  highestStudentList: [],
  lowestStudentList: [],
  rankingByCityList: [],
};
const dashboardSlice = createSlice({
  name: 'dashboardSlice',
  initialState,
  reducers: {
    fetchData: (state) => {
      state.loading = true;
    },
    fetchDataSuccess: (state) => {
      state.loading = false;
    },
    fetchDataFail: (state) => {
      state.loading = false;
    },
    getDataStatistic: (state, action: PayloadAction<StatisticsProps>) => {
      state.statistics = action.payload;
      console.log('action.payload: ', action.payload);
    },
    getDataHighestStudentList: (state, action: PayloadAction<Student[]>) => {
      state.highestStudentList = action.payload;
    },
    getDataLowestStudentList: (state, action: PayloadAction<Student[]>) => {
      state.lowestStudentList = action.payload;
    },
    getDataRankingByCity: (state, action: PayloadAction<RankingByCityProps[]>) => {
      state.rankingByCityList = action.payload;
    },
  },
});

export const {
  fetchData,
  fetchDataSuccess,
  fetchDataFail,
  getDataHighestStudentList,
  getDataLowestStudentList,
  getDataRankingByCity,
  getDataStatistic,
} = dashboardSlice.actions;
export default dashboardSlice.reducer;
