import { useEffect } from 'react';
import { fetchData } from './dashboardSlice';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import cityApi from 'api/citiApi';
export default function Dashboard() {
  const data = useAppSelector((state) => state.dashboardSlice.highestStudentList);
  console.log('data: ', data);
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(fetchData());
  }, [dispatch]);

  return <div>hello</div>;
}
