import { all, call, put, takeLatest } from 'redux-saga/effects';
import {
  fetchData,
  fetchDataSuccess,
  getDataHighestStudentList,
  getDataLowestStudentList,
  getDataRankingByCity,
  getDataStatistic,
} from './dashboardSlice';
import { City, Student, ListResponse, RankingByCityProps } from 'models';
import studentApi from 'api/studentApi';
import cityApi from 'api/citiApi';
function* getStatisticData() {
  const response: Array<ListResponse<Student>> = yield all([
    call(studentApi.getStudentList, { _page: 1, _limit: 1, gender: 'male' }),
    call(studentApi.getStudentList, { _page: 1, _limit: 1, gender: 'female' }),
    call(studentApi.getStudentList, { _page: 1, _limit: 1, mark_gte: 8 }),
    call(studentApi.getStudentList, { _page: 1, _limit: 1, mark_lte: 5 }),
  ]);
  const statisticList = response.map((x) => x.pagination._totalRows);
  const [male, female, highMark, lowMark] = statisticList;
  yield put(getDataStatistic({ male, female, highMark, lowMark }));
}
function* getHighestStudentMark() {
  const { data }: ListResponse<Student> = yield call(studentApi.getStudentList, {
    _page: 1,
    _limit: 5,
    _sort: 'mark',
    _order: 'desc',
  });
  yield put(getDataHighestStudentList(data));
}
function* getLowestStudentMark() {
  const { data }: ListResponse<Student> = yield call(studentApi.getStudentList, {
    _page: 1,
    _limit: 5,
    _sort: 'mark',
    _order: 'asc',
  });
  yield put(getDataLowestStudentList(data));
}
function* getRankingByCity() {
  const { data }: ListResponse<City> = yield call(cityApi.getAllCity);
  const callList = data.map((x) =>
    call(studentApi.getStudentList, {
      _page: 1,
      _limit: 5,
      _sort: 'mark',
      _order: 'desc',
      city: x.code,
    })
  );
  const responseList: ListResponse<Student>[] = yield all(callList);
  const rankingByCity: Array<RankingByCityProps> = responseList.map((e, i) => ({
    cityId: data[i].code,
    rankingList: e.data,
  }));
  yield put(getDataRankingByCity(rankingByCity));
}

function* fetchDashboardData() {
  try {
    yield all([
      call(getStatisticData),
      call(getHighestStudentMark),
      call(getLowestStudentMark),
      call(getRankingByCity),
    ]);
    yield put(fetchDataSuccess());
  } catch (error) {
    console.log('error: ', error);
  }
}

export default function* dashboardSaga() {
  yield takeLatest(fetchData.type, fetchDashboardData);
}
