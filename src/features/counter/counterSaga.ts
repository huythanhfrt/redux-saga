import { takeEvery, put, delay } from 'redux-saga/effects';
import { incrementSaga, incrementSagaSuccess } from './counterSlice';
import { PayloadAction } from '@reduxjs/toolkit';
function* handleIncrementSagaSuccess(action: PayloadAction<number>) {
  yield delay(1000);
  yield put(incrementSagaSuccess(action.payload));
}
export default function* counterSaga() {
  yield takeEvery(incrementSaga.toString(), handleIncrementSagaSuccess);
}
