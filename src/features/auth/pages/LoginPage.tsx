import { useAppDispatch, useAppSelector } from 'app/hooks';
import { useSelector } from 'react-redux';
import { logOut, login } from '../authSlice';
import { useHistory } from 'react-router-dom';

export interface LoginPageProps {}

export default function LoginPage() {
  const isLoading = useAppSelector((state) => state.authSlice.isLoading);
  console.log('isLoading: ', isLoading);
  const history = useHistory();

  const dispatch = useAppDispatch();
  const handleLogin = () => {
    dispatch(login({ username: 'admin', password: '123456' }));
    console.log('change route');
    history.push('/notfound');
  };
  const handleLogout = () => {
    dispatch(logOut());
  };
  return (
    <div>
      <h1>LoginPage</h1>
      <button
        onClick={() => {
          handleLogin();
        }}
      >
        Login
      </button>
      <button
        onClick={() => {
          handleLogout();
        }}
      >
        Logout
      </button>
      <div
        onChange={(e) => {
          console.log(e.target);
        }}
        id="label-input"
      >
        <input type="checkbox" name="Iphone" value="0" id="" />
        <input type="checkbox" name="Samsung" value="1" id="" />
        <input type="checkbox" name="Oppo" value="2" id="" />
      </div>
    </div>
  );
}
