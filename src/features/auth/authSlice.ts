import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { User } from 'models';
export interface UserInfo {
  username: string;
  password: string;
}
export interface AuthState {
  isLogin: boolean;
  isLoading: boolean;
  currentUser?: User;
}
const initialState: AuthState = {
  isLogin: false,
  isLoading: false,
  currentUser: undefined,
};
const authSlice = createSlice({
  name: 'authSlice',
  initialState,
  reducers: {
    login: (state, action: PayloadAction<UserInfo>) => {
      state.isLoading = true;
    },
    loginSuccess: (state, { payload }: PayloadAction<User>) => {
      state.isLoading = false;
      state.currentUser = payload;
    },
    loginFail: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
    },
    logOut: (state) => {
      state.isLogin = false;
      state.currentUser = undefined;
    },
  },
});
export const { login, loginSuccess, loginFail, logOut } = authSlice.actions;
export default authSlice.reducer;
