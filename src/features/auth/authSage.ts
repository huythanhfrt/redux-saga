import { PayloadAction } from '@reduxjs/toolkit';
import { UserInfo, logOut, login, loginFail, loginSuccess } from './authSlice';
import { call, fork, put, take } from 'redux-saga/effects';

function* handleLogin(payload: UserInfo) {
  localStorage.setItem('access_token', '9594954954');
  yield put(loginSuccess({ id: '1', name: 'thành' }));
  console.log('done');
}
function* handleLogout() {
  localStorage.removeItem('access_token');
}
function* watchLoginFlow() {
  while (true) {
    const isLogin = localStorage.getItem('access_token');
    if (!isLogin) {
      const action: PayloadAction<UserInfo> = yield take(login.type);
      yield fork(handleLogin, action.payload);
    }
    yield take([logOut.type, loginFail.type]);
    yield call(handleLogout);
  }
}

export function* authSaga() {
  yield fork(watchLoginFlow);
}
