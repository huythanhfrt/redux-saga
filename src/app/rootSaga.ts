import { PayloadAction } from '@reduxjs/toolkit';
import { delay } from '@reduxjs/toolkit/dist/utils';
import { authSaga } from 'features/auth/authSage';
import counterSaga from 'features/counter/counterSaga';
import { increment, incrementSaga } from 'features/counter/counterSlice';
import dashboardSaga from 'features/dashboard/dashboardSaga';
import { all, takeEvery } from 'redux-saga/effects';
function* Log(action: PayloadAction) {
  console.log(action);
}

export default function* rootSaga() {
  yield all([counterSaga(), authSaga(), dashboardSaga()]);
  // all chạy toàn bộ các saga trong lần render đầu tiên
}
