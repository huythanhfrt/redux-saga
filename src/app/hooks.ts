import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import type { RootState, AppDispatch } from './store';

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
/*
  mỗi lần sử dụng useSelector thì phải khai báo type cho state
  const isLoading = useSelector((state:any) => state.authSlice.isLoading);

  còn useDispatch do 1 app có thể sử dụng nhiều middleware khác nhau mà store đang sử dụng

*/
